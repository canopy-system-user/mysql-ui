1. Clone the repository on your local machine.
2. push the app
   cf push <app-name>
3. Bind your mysql service with:
   cf bs appname <service_name>
4. Restage the service with:
   cf restage phpmyadmin-cfready
5. Get username & password for login to GUI:
   cf env <any_app_name_bound_to_your_service>
